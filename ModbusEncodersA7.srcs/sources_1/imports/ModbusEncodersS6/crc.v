`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:00:13 01/30/2020 
// Design Name: 
// Module Name:    crc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module crc(
	input clk,
	input reset,
	input [7:0] word,
	input wordValid,
	output reg [15:0] crc = 16'hFFFF
	);

	reg [2:0] ctr = 3'h0;
	reg busy = 1'b0;
	always @ (posedge clk)
	begin
		// initialize CRC before first word
		if (reset)
		begin
			crc <= 16'hFFFF;
			busy <= 1'b0;
			ctr <= 3'h0;
		end
		// update CRC with data byte
		else
		begin
			if (wordValid)
			begin
				crc <= crc ^ {8'h00, word};
				busy <= 1'b1;
				ctr <= 3'h0;
			end
			else if (busy)
			begin
				crc <= crc[0] ? (crc >> 1) ^ 16'hA001 : crc >> 1;
				ctr <= ctr + 1'b1;
				if (&ctr)
					busy <= 1'b0;
			end
		end
	end
endmodule
